package com.demo.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demo.model.Persona;
import com.demo.service.IPersonaService;

@WebServlet("/PersonaController")
public class PersonaServlet extends HttpServlet{

	public static final String LIST_PERSONA = "/listaPersona.jsp";
	public static final String INSERT_EDIT = "/persona.jsp";
	
	@Inject //identifica un punto el cual una dependencia en una clase o interfaz Java puede ser inyectada en una clase destino
	private IPersonaService service;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String redireccion = "";
		String accion = req.getParameter("accion");
		
		if (accion.equalsIgnoreCase("ELIMINAR")) {
			redireccion = LIST_PERSONA;
			int id = Integer.parseInt(req.getParameter("id"));
			service.eliminar(id);

			try {
				req.setAttribute("personas", service.listarTodos());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (accion.equalsIgnoreCase("EDITAR")) {
			redireccion = INSERT_EDIT;
			int id = Integer.parseInt(req.getParameter("id"));
			Persona persona;
			try {
				persona = service.buscar(id);
				req.setAttribute("persona", persona);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if (accion.equalsIgnoreCase("INSERTAR")) {
			redireccion = INSERT_EDIT;
		} else {
			redireccion = LIST_PERSONA;
			try {
				req.setAttribute("personas", service.listarTodos());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		RequestDispatcher vista = req.getRequestDispatcher(redireccion);
		vista.forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		

		Persona persona = new Persona();
		persona.setNombre(req.getParameter("nombre"));
		persona.setEmail(req.getParameter("email"));

		String id = req.getParameter("id");

		if (id == null || id.isEmpty()) {
			try {
				service.registrar(persona);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else {
			persona.setId(Integer.parseInt(id));
			try {
				service.actualizar(persona);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		RequestDispatcher vista = req.getRequestDispatcher(LIST_PERSONA);
		try {
			req.setAttribute("personas", service.listarTodos());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		vista.forward(req, resp);
	}
	
}
