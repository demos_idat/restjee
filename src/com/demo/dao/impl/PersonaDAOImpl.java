package com.demo.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import com.demo.dao.Conexion;
import com.demo.dao.IPersonaDAO;
import com.demo.model.Persona;


@Stateless  //El estado del bean no tiene datos para un cliente en espec�fico.
//@stateful  // El bean necesita mantener informaci�n acerca del cliente a lo largo de varias invocaciones de m�todos.
//@singleton  //El estado necesita ser compartido a trav�s de la aplicaci�n
public class PersonaDAOImpl implements IPersonaDAO{
	
	private Connection cx;
	
	public PersonaDAOImpl() {
		cx = Conexion.conectar();
	}

	@Override
	public Persona guardar(Persona persona) {
		try {
			String sql = "INSERT INTO PERSONA(nombres, email) VALUES(?, ?)";
			PreparedStatement preparedStatement = cx.prepareStatement(sql);
			preparedStatement.setString(1, persona.getNombre());
			preparedStatement.setString(2, persona.getEmail());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			int generatedKey = 0;
			if (resultSet.next()) {
			    generatedKey = resultSet.getInt(1);
			}
			resultSet.close();
			persona.setId(generatedKey);
			preparedStatement.close();
			return persona;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
		
	}

	@Override
	public Persona actualizar(Persona persona) {
		try {
			String sql = "UPDATE PERSONA SET nombres = ? , email = ? WHERE id = ?";
			PreparedStatement preparedStatement = cx.prepareStatement(sql);
			preparedStatement.setString(1, persona.getNombre());
			preparedStatement.setString(2, persona.getEmail());
			preparedStatement.setInt(3, persona.getId());
			preparedStatement.executeUpdate();
			preparedStatement.close();
			return persona;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	@Override
	public Persona buscarPorId(int id) {
		Persona persona = null;
		try {
			String sql = "SELECT id, nombres, email FROM PERSONA WHERE id = ?";
			PreparedStatement preparedStatement = cx.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				persona = new Persona(
						resultSet.getInt("id"), 
						resultSet.getString("nombres"), 
						resultSet.getString("email"));
			}
			resultSet.close();
			preparedStatement.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return persona;
	}

	@Override
	public List<Persona> listar() {
		List<Persona> personas = new ArrayList<>();
		try {
			String sql = "SELECT id, nombres, email FROM PERSONA";
			PreparedStatement preparedStatement = cx.prepareStatement(sql);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				personas.add( new Persona(
						resultSet.getInt("id"), 
						resultSet.getString("nombres"), 
						resultSet.getString("email")));
			}
			resultSet.close();
			preparedStatement.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			personas = new ArrayList<>();
		}
		return personas;
	}

	@Override
	public void eliminar(int id) {
		try {
			String sql = "DELETE FROM PERSONA WHERE ID = ?";
			PreparedStatement preparedStatement = cx.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void delete2(int id) throws SQLException {
		PreparedStatement preparedStatementDelete = null;
		PreparedStatement preparedStatementupdate = null;
		try {
			cx.setAutoCommit(false); //Inicio del bloque transaccionnal
			String sql_delete = "DELETE FROM PERSONA WHERE ID = ?";
			String sql_update = "UPDATE PERSONA_ SET nombres = ? , email = ? WHERE id = ?";
			
			preparedStatementDelete = cx.prepareStatement(sql_delete);
			preparedStatementDelete.setInt(1, id);
			preparedStatementDelete.executeUpdate();
			
			preparedStatementupdate = cx.prepareStatement(sql_update);
			preparedStatementupdate.setString(1, null);
			preparedStatementupdate.setString(2, null);
			preparedStatementupdate.setInt(3, id);
			preparedStatementupdate.executeUpdate();
			
			cx.commit();
			
			
		} catch (Exception e) {
			e.printStackTrace();
			cx.rollback();
		} finally{
			if(preparedStatementDelete != null) {
				preparedStatementDelete.close();
			}
			if(preparedStatementupdate != null) {
				preparedStatementupdate.close();
			}
			
		}
	}

}
