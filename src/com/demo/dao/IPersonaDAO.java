package com.demo.dao;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Local;

import com.demo.model.Persona;

@Local //@Remote 
public interface IPersonaDAO {
	
	Persona guardar(Persona persona);
	
	Persona actualizar(Persona persona);
	
	Persona buscarPorId(int id);
	
	List<Persona> listar();
	
	void eliminar(int id);

	void delete2(int id) throws SQLException;

}
