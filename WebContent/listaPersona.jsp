<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lista de Personas</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
</head>
</head>
<body>
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr class="info">
					<th>ID</th>
					<th>Nombres</th>
					<th>Email</th>
					<th colspan="2">Acci�n</th>
				</tr>
			</thead>
			<tbody>
						
			<c:forEach items="${personas}" var="per">
				<tr>
					<td><c:out value="${per.id}"></c:out></td>
					<td><c:out value="${per.nombre}"></c:out></td>
					<td><c:out value="${per.email}"></c:out></td>
					<td><a href="PersonaController?accion=editar&id=<c:out value="${per.id}"/>">Actualizar</a></td>
					<td><a href="PersonaController?accion=eliminar&id=<c:out value="${per.id}"/>">Eliminar</a></td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>

	<form method="get" action="PersonaController">
		<input type="hidden" name="accion" value="insertar">
		<button class="btn-primary">Agregar</button>
	</form>
	
	<a href="PersonaController?accion=insertar">Insertar</a>

</body>
</html>